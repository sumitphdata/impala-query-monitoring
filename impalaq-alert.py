#!/usr/bin/env python3
"""
This is a script put together to pull Impala query statistics and send an email to customer for impala queries running more than one hour
"""
#impala-qmonitor querymonitor 

import argparse
from configparser import ConfigParser
import json
import datetime
import copy
import os.path
import numpy as np
import requests
import urllib3
urllib3.disable_warnings()




def query_running_from_more_than_1hr(cloudera_api, query_duration):


    # api_url = '%s://%s:%s/api/%s/clusters/%s/services/impala/impalaQueries?filter=query_duration%3E%s&queryState=RUNNING' % \
    #         (cloudera_api['protocol'],
    #          cloudera_api['hostname'],
    #          cloudera_api['port'],
    #          cloudera_api['version'],
    #          cloudera_api['cluster'],
    #          query_duration)    

    api_url = '%s://%s:%s/api/%s/clusters/%s/services/impala/impalaQueries' % \
            (cloudera_api['protocol'],
             cloudera_api['hostname'],
             cloudera_api['port'],
             cloudera_api['version'],
             cloudera_api['cluster']) 



    # offset = 0        
    # while True:
    #     # looping and pulling 1,000 queries at a time
    #     tmp_url = api_url + '&offset=' + str(offset)
    #     offset += 1000
    #     response = requests.get(tmp_url, auth=(cloudera_api['username'], cloudera_api['password']), verify=False)
    #     if response.status_code != 200:
    #         exit(response.text)

    tmp_url = api_url
    print tmp_url
    response = requests.get(tmp_url, auth=(cloudera_api['username'], cloudera_api['password']), verify=False)
    if response.status_code != 200:
        exit(response.text)
        
    json_data = json.loads(response.text)

    # if not json_data['queries']:
    #     # we have ran out of pages to go through
    #     break

    for query in json_data['queries']:
		print query
#      print query['queryId'], query['user'], 
      

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", action="store", dest="config",
                        required=True, help="path to your configuration file")

    args = parser.parse_args()

    # Does the config file actually exist?
    if os.path.exists(args.config) is False:
        exit('invalid config file')

    # Create parser and read ini configuration file
    parser = ConfigParser()
    parser.read(args.config)

    # Get config section
    cloudera_api = {}
    items = parser.items('config')
    for item in items:
        cloudera_api[item[0]] = item[1]

    query_running_from_more_than_1hr(cloudera_api, 1)


